﻿using Xunit;
using Yappy.Business.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Yappy.Business.Factories;
using Yappy.Business.Infrastructure;

namespace Yappy.Business.Services.Tests
{
    public class StitchServiceTest
    {
        [Fact()]
        public async void StichImagesTest()
        {
            var whiteTile = Convert.FromBase64String( "R0lGODlhCgAKAPAAAP///wAAACH5BAAAAAAALAAAAAAKAAoAAAIIhI+py+0PYysAOw==");
            var redTile = Convert.FromBase64String("R0lGODlhBQAFAPAAAP8AAAAAACH5BAAAAAAALAAAAAAFAAUAAAIEhI+pWAA7");

            var storeMock = new Mock<IFileStore>();
            storeMock.Setup(x=> x.GetFile("white")).ReturnsAsync(whiteTile);
            storeMock.Setup(x=> x.GetFile("red")).ReturnsAsync(redTile);

            var storeFactMock = new Mock<IFileStoreFactory>();
            storeFactMock.Setup(x=>x.GetFileStore( It.IsAny<FileStoreTypes>() )).Returns(storeMock.Object);


            var stitch = new StitchService(storeFactMock.Object);

            var result = await stitch.StichImages(new List<ImageInfo>
            {
                new ImageInfo { Path = "white", Store = FileStoreTypes.Http},
                new ImageInfo { Path = "red", Store = FileStoreTypes.Http, Width=5, X=2, Y=2 },
            });

            Assert.NotNull(result);
        }
    }
}