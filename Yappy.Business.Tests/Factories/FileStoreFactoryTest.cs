﻿using Xunit;
using Yappy.Business.Factories;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Yappy.Business.Services;

namespace Yappy.Business.Factories.Tests
{
    public class FileStoreFactoryTest
    {
        [Fact()]
        public void UsingCorrectFileStoreType_Should_ReturnsCorrectFileStoreService()
        {
            var serviceProvider = new Mock<IServiceProvider>();
            serviceProvider
                .Setup(x => x.GetService(typeof(IFileStoreLocal)))
                .Returns( new Mock<IFileStoreLocal>().Object );
            serviceProvider
                .Setup(x => x.GetService(typeof(IFileStoreHttp)))
                .Returns(new Mock<IFileStoreHttp>().Object);

            FileStoreFactory fileStoreFactory = new FileStoreFactory(serviceProvider.Object);


            var getHttp = fileStoreFactory.GetFileStore(Infrastructure.FileStoreTypes.Http);
            var getLocal = fileStoreFactory.GetFileStore(Infrastructure.FileStoreTypes.Local);

            Assert.IsAssignableFrom<IFileStoreHttp>(getHttp);
            Assert.IsAssignableFrom<IFileStoreLocal>(getLocal);
        }

        [Fact()]
        public void UsingIncorrectFileStoreType_Should_ReturnsWrongFileStoreService()
        {
            var serviceProvider = new Mock<IServiceProvider>();
            serviceProvider
                .Setup(x => x.GetService(typeof(IFileStoreLocal)))
                .Returns(new Mock<IFileStoreLocal>().Object);
            serviceProvider
                .Setup(x => x.GetService(typeof(IFileStoreHttp)))
                .Returns(new Mock<IFileStoreHttp>().Object);

            FileStoreFactory fileStoreFactory = new FileStoreFactory(serviceProvider.Object);


            var getHttp = fileStoreFactory.GetFileStore(Infrastructure.FileStoreTypes.Local);
            var getLocal = fileStoreFactory.GetFileStore(Infrastructure.FileStoreTypes.Http);

            Assert.False(getHttp is IFileStoreHttp);
            Assert.False(getLocal is IFileStoreLocal);
        }
    }
}