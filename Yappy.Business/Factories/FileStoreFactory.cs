﻿using System;
using System.Collections.Generic;
using System.Text;
using Yappy.Business.Infrastructure;
using Yappy.Business.Services;

namespace Yappy.Business.Factories
{
    public class FileStoreFactory : IFileStoreFactory
    {
        private readonly IServiceProvider serviceProvider;

        public FileStoreFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public virtual IFileStore GetFileStore(FileStoreTypes fsType)
        {
            switch (fsType)
            {
                //Differnet local stores could be injected along with different http stores
                //Of course if this flexibily would never be needed there maybe no point to do this.
                case FileStoreTypes.Local:
                    return (IFileStore)serviceProvider.GetService(typeof(IFileStoreLocal));
                case FileStoreTypes.Http:
                    return (IFileStore)serviceProvider.GetService(typeof(IFileStoreHttp));
                default:
                    throw new Exception("Unknown file store type");
            }
        }
    }
}
