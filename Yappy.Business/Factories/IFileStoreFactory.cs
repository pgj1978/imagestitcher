﻿using Yappy.Business.Infrastructure;
using Yappy.Business.Services;

namespace Yappy.Business.Factories
{
    public interface IFileStoreFactory
    {
        IFileStore GetFileStore(FileStoreTypes fsType);
    }
}