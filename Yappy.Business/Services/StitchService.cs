﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Flurl.Http;
using Yappy.Business.Factories;
using Yappy.Business.Infrastructure;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace Yappy.Business.Services
{
    public class StitchService : IStitchService
    {
        private readonly IFileStoreFactory fileStoreFactory;

        public StitchService(IFileStoreFactory fileStoreFactory )
        {
            this.fileStoreFactory = fileStoreFactory;
        }

        public async Task<byte[]> StichImages(ImageInfo background, IList<ImageInfo> forgrounds)
        {
            forgrounds.Insert(0, background);

            return await this.StichImages(forgrounds);
        }

        public async Task<byte[]> StichImages(IList<ImageInfo> images)
        {
            Image comp = null;
            foreach ( var image in images)
            {
                var storeService = this.fileStoreFactory.GetFileStore(image.Store);
                if (comp == null)
                    comp = Image.Load(await storeService.GetFile(image.Path));
                else
                {
                    var overlay = Image.Load(await storeService.GetFile(image.Path));

                    if(overlay.Width != image.Width)
                    {
                        decimal scale = (decimal)image.Width / (decimal)overlay.Width;
                        int newWidth = (int)Math.Round (overlay.Width * scale);
                        int newHeight = (int)Math.Round(overlay.Width * scale);
                        overlay.Mutate(o => o.Resize(newWidth, newHeight));
                    }

                    comp.Mutate(o => o.DrawImage(overlay, new Point(image.X, image.Y), 1f));
                }
            }

            using (var ms = new MemoryStream())
            {
                await comp.SaveAsJpegAsync(ms);
                return ms.ToArray();
            }
        }
    }
}
