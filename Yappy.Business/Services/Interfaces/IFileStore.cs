﻿using System.Threading.Tasks;

namespace Yappy.Business.Services
{
    public interface IFileStore
    {
        Task<byte[]> GetFile(string path);
    }

    public interface IFileStoreHttp: IFileStore
    {

    }

    public interface IFileStoreLocal: IFileStore
    {

    }
}