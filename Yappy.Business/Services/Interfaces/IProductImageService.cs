﻿using System.Threading.Tasks;

namespace Yappy.Business.Services
{
    public interface IProductImageService
    {
        Task<byte[]> GetMainImage(int productId);
    }
}