﻿using System.Threading.Tasks;

namespace Yappy.Business.Services
{
    public interface IDataStore
    {
        Task<ProductInfo> GetProductInfo(int productId);
    }
}