﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Yappy.Business.Infrastructure;

namespace Yappy.Business.Services
{
    public interface IStitchService
    {
        Task<byte[]> StichImages(ImageInfo background, IList<ImageInfo> forgrounds);
        Task<byte[]> StichImages(IList<ImageInfo> images);
    }
}