﻿using Flurl.Http;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Yappy.Business.Services
{
    public class FileStoreHttp : IFileStoreHttp
    {
        public async Task<byte[]> GetFile(string path)
        {
            return await path.GetBytesAsync();
        }
    }

    public class FileStoreLocal : IFileStoreLocal
    {
        private readonly IHostingEnvironment hostingEnvironment;

        public FileStoreLocal(IHostingEnvironment hostingEnvironment )
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        public async Task<byte[]> GetFile(string path)
        {
            return await File.ReadAllBytesAsync( $"{this.hostingEnvironment.ContentRootPath}\\assets\\{path}");
        }
    }
}
