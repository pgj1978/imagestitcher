﻿using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Yappy.Business.Services
{
    public class ProductImageService : IProductImageService
    {
        private readonly IDataStore dataStore;
        private readonly IStitchService stitch;

        public ProductImageService(IDataStore dataStore, IStitchService stitch)
        {
            this.dataStore = dataStore;
            this.stitch = stitch;
        }

        public async Task<byte[]> GetMainImage(int productId)
        {
            var product = await this.dataStore.GetProductInfo(productId);

            var result = await this.stitch.StichImages( product.MainImage.Background, product.MainImage.Foregrounds);

            return result;
        }
    }
}
