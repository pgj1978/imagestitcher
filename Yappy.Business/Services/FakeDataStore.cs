﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yappy.Business.Infrastructure;

namespace Yappy.Business.Services
{

    /// <summary>
    /// This is a dummy data store for gettign product info
    /// </summary>
    public class FakeDataStore : IDataStore
    {

        public async Task<ProductInfo> GetProductInfo(int productId)
        {
            var products = new[]
            {
                new ProductInfo { ProductId = 1,
                    MainImage = new ImageCompilation  {
                        Background = new ImageInfo { Path = "Background.jpg", Store = FileStoreTypes.Local },
                        Foregrounds = new List<ImageInfo>
                        {
                            new ImageInfo { Path = "https://images.yappy.com/yappicon/greatdane/greatdane-04.png", Store = FileStoreTypes.Http, X=315, Y=266, Width =257 } // Main Dog
                        }
                    }
                },

                new ProductInfo { ProductId = 2,
                    MainImage = new ImageCompilation  {
                        Background = new ImageInfo { Path = "Background2.jpg", Store = FileStoreTypes.Local },
                        Foregrounds = new List<ImageInfo>
                        {
                            new ImageInfo { Path = "https://images.yappy.com/yappicon/greatdane/greatdane-02.png", Store = FileStoreTypes.Http, X=315, Y=266, Width =257 }, //Different dog
                            new ImageInfo { Path = "https://pngimg.com/uploads/glasses/glasses_PNG54332.png", Store = FileStoreTypes.Http, X=385, Y=286, Width =120 }  // Glasses for the dog
                        }
                    }
                }
            };

            await Task.Delay(100);  // Simulate loading data.

            return products.Where(p => p.ProductId == productId).FirstOrDefault();
        }
    }

    public class ProductInfo
    {
        public int ProductId { get; set; }
        public ImageCompilation MainImage { get; set; }
    }
}
