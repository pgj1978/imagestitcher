﻿using System.Collections.Generic;

namespace Yappy.Business.Infrastructure
{
    public class ImageCompilation
    {
        public ImageInfo Background { get; set; }
        public IList<ImageInfo> Foregrounds { get; set; }
    }
}
