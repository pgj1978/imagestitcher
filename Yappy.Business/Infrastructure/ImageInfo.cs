﻿namespace Yappy.Business.Infrastructure
{
    public class ImageInfo
    {
        public string Path { get; set; }
        public FileStoreTypes Store { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
    }
}
