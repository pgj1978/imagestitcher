﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yappy.Business.Services;

namespace ImageStitcher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly IProductImageService productImage;

        public ImageController(IProductImageService productImage)
        {
            this.productImage = productImage;
        }
         

        [HttpGet("{productId}/main-product-image")] 
        public async Task<IActionResult> MainProductImage( int productId )
        {

            return File(await this.productImage.GetMainImage(productId), "image/jpg");
        }
    }
}
